﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mmdo.src.Cal
{
    public enum TableAnswerType { Unbounded, Found, NotYetFound }

    public class SmplxMethod
    {
        Fun function;

        double[] functionVariables;
        double[][] matrix;
        double[] b;
        bool[] m;
        double[] M;
        double[] F;
        int[] C;
        bool mBool = false;


        public SmplxMethod(Fun function, Contr[] constraints)
        {
            if (function.isExtrMax)
            {
                this.function = function;
            }
            else
            {
                this.function = Canonize(function);
            }

            getMatrix(constraints);
            getFunctionArray();
            IndexRowCalculations();

            for (int i = 0; i < F.Length; i++)
            {
                F[i] = -functionVariables[i];
            }

        }

        public SmplxMethod(SmplxTable screenshot, Fun function)
        {
            this.b = screenshot.b;
            this.matrix = screenshot.matrix;
            this.M = screenshot.M;
            this.F = screenshot.F;
            this.C = screenshot.C;
            this.functionVariables = screenshot.fVars;
            this.m = screenshot.m;

            this.function = function;


            getFunctionArray();
            IndexRowCalculations();

            for (int i = 0; i < F.Length; i++)
            {
                F[i] = -functionVariables[i];
            }


        }

        public Fun GetFunction() { return function; }


        public Tuple<List<SmplxTable>, TableAnswerType> GetIterations()
        {
            List<SmplxTable> list = new List<SmplxTable>();

            var initTable = new SmplxTable(b, matrix, M, F, C, functionVariables, mBool, m);
            list.Add(initTable);

            Pivot pivot = GetRowColItem();
            int i = 0;


            while (pivot.result == TableAnswerType.NotYetFound && i < 100)
            {
                calculate(pivot.index);
                SmplxTable table = new SmplxTable(b, matrix, M, F, C, functionVariables, mBool, m);
                pivot = GetRowColItem();

                list.Add(table);
                i++;
            }

            return new Tuple<List<SmplxTable>, TableAnswerType> (list, pivot.result);
        }



        void calculate(Tuple<int, int> Xij)
        {
            double[][] newMatrix = new double[matrix.Length][];

            C[Xij.Item2] = Xij.Item1;

            double[] newJRow = new double[matrix.Length];

            for (int i = 0; i < matrix.Length; i++)
            {
                newJRow[i] = matrix[i][Xij.Item2] / matrix[Xij.Item1][Xij.Item2];
            }

            double[] newB = new double[b.Length];

            for (int i = 0; i < b.Length; i++)
            {
                if (i == Xij.Item2)
                {
                    newB[i] = b[i] / matrix[Xij.Item1][Xij.Item2];
                }
                else
                {
                    newB[i] = b[i] - b[Xij.Item2] / matrix[Xij.Item1][Xij.Item2] * matrix[Xij.Item1][i];
                }
            }

            b = newB;

            for (int i = 0; i < matrix.Length; i++)
            {
                newMatrix[i] = new double[C.Length];
                for (int j = 0; j < C.Length; j++)
                {
                    if (j == Xij.Item2)
                    {
                        newMatrix[i][j] = newJRow[i];
                    }
                    else
                    {
                        newMatrix[i][j] = matrix[i][j] - newJRow[i] * matrix[Xij.Item1][j];
                    }
                }
            }

            matrix = newMatrix;
            IndexRowCalculations();
        }

        void IndexRowCalculations()
        {
            M = new double[matrix.Length];
            F = new double[matrix.Length];

            for (int i = 0; i < matrix.Length; i++)
            {
                double sumF = 0;
                double sumM = 0;
                for (int j = 0; j < matrix.First().Length; j++)
                {
                    if (m[C[j]])
                    {
                        sumM -= matrix[i][j];
                    }
                    else
                    {
                        sumF += functionVariables[C[j]] * matrix[i][j];
                    }
                }
                M[i] = m[i] ? sumM + 1 : sumM;
                F[i] = sumF - functionVariables[i];
            }
        }

        Pivot GetRowColItem()
        {

            int columnM = FindCol(M);

            if (mBool || columnM == -1)
            {
                mBool = true;
                int columnF = FindCol(F);

                if (columnF != -1)
                {
                    int row = FindRow(matrix[columnF], b);

                    if (row != -1)
                        return new Pivot(new Tuple<int, int>(columnF, row), TableAnswerType.NotYetFound);
                    else
                        return new Pivot(null, TableAnswerType.Unbounded);
                }
                else
                    return new Pivot(null, TableAnswerType.Found);
            }
            else
            {
                int row = FindRow(matrix[columnM], b);

                if (row != -1)
                    return new Pivot(new Tuple<int, int>(columnM, row), TableAnswerType.NotYetFound);
                else
                    return new Pivot(null, TableAnswerType.Unbounded);
            }
        }

        int FindCol(double[] array)
        {
            int index = -1;

            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] < 0)
                {
                    if (!mBool || mBool && !m[i])
                    {
                        if (index == -1)
                        {
                            index = i;
                        }
                        else if (Math.Abs(array[i]) > Math.Abs(array[index]))
                        {
                            index = i;
                        }
                    }

                }
            }
            return index;
        }

        int FindRow(double[] column, double[] b)
        {
            int index = -1;

            for (int i = 0; i < column.Length; i++)
            {
                if (column[i] > 0 && b[i] > 0)
                {
                    if (index == -1)
                    {
                        index = i;
                    }
                    else if (b[i] / column[i] < b[index] / column[index])
                    {
                        index = i;
                    }
                }
            }

            return index;
        }

        public void getFunctionArray()
        {
            double[] funcVars = new double[matrix.Length];
            for (int i = 0; i < matrix.Length; i++)
            {
                funcVars[i] = i < function.variables.Length ? function.variables[i] : 0;
            }
            this.functionVariables = funcVars;
        }

        public Fun Canonize(Fun function)
        {
            double[] newFuncVars = new double[function.variables.Length];

            for (int i = 0; i < function.variables.Length; i++)
            {
                newFuncVars[i] = -function.variables[i];
            }
            return new Fun(newFuncVars, true);
        }

        double[][] appendColumn(double[][] matrix, double[] column)
        {
            double[][] newMatrix = new double[matrix.Length + 1][];
            for (int i = 0; i < matrix.Length; i++)
            {
                newMatrix[i] = matrix[i];
            }
            newMatrix[matrix.Length] = column;
            return newMatrix;
        }

        T[] append<T>(T[] array, T element)
        {
            T[] newArray = new T[array.Length + 1];
            for (int i = 0; i < array.Length; i++)
            {
                newArray[i] = array[i];
            }
            newArray[array.Length] = element;
            return newArray;
        }

        double[] getColumn(double value, int place, int length)
        {
            double[] newColumn = new double[length];

            for (int k = 0; k < length; k++)
            {
                newColumn[k] = k == place ? value : 0;
            }

            return newColumn;
        }

        public void getMatrix(Contr[] constraints)
        {
            for (int i = 0; i < constraints.Length; i++)
            {
                if (constraints[i].b < 0)
                {
                    double[] cVars = new double[constraints[i].vars.Length];

                    for (int j = 0; j < constraints[i].vars.Length; j++)
                    {
                        cVars[j] = -constraints[i].vars[j];
                    }

                    string sign = constraints[i].sign;

                    if (sign == ">=")
                    {
                        sign = "<=";
                    }
                    else if (sign == "<=")
                    {
                        sign = ">=";
                    }

                    Contr cNew = new Contr(cVars, -constraints[i].b, sign);
                    constraints[i] = cNew;
                }
            }

            double[][] matrix = new double[constraints.First().vars.Length][];

            for (int i = 0; i < constraints.First().vars.Length; i++)
            {
                matrix[i] = new double[constraints.Length];
                for (int j = 0; j < constraints.Length; j++)
                {
                    matrix[i][j] = constraints[j].vars[i];
                }
            }

            double[][] appendixMatrix = new double[0][];
            double[] Bs = new double[constraints.Length];

            for (int i = 0; i < constraints.Length; i++)
            {
                Contr current = constraints[i];

                Bs[i] = current.b;

                if (current.sign == ">=")
                {
                    appendixMatrix = appendColumn(appendixMatrix, getColumn(-1, i, constraints.Length));
                }
                else if (current.sign == "<=")
                {
                    appendixMatrix = appendColumn(appendixMatrix, getColumn(1, i, constraints.Length));
                }
            }

            double[][] newMatrix = new double[constraints.First().vars.Length + appendixMatrix.Length][];

            for (int i = 0; i < constraints.First().vars.Length; i++)
            {
                newMatrix[i] = matrix[i];
            }

            for (int i = constraints.First().vars.Length; i < constraints.First().vars.Length + appendixMatrix.Length; i++)
            {
                newMatrix[i] = appendixMatrix[i - constraints.First().vars.Length];
            }

            bool[] hasBasicVar = new bool[constraints.Length];

            for (int i = 0; i < constraints.Length; i++)
            {
                hasBasicVar[i] = false;
            }

            C = new int[constraints.Length];

            int ci = 0;
            for (int i = 0; i < newMatrix.Length; i++)
            {


                bool hasOnlyNulls = true;
                bool hasOne = false;
                Tuple<int, int> onePosition = new Tuple<int, int>(0, 0);
                for (int j = 0; j < constraints.Length; j++)
                {


                    if (newMatrix[i][j] == 1)
                    {
                        if (hasOne)
                        {
                            hasOnlyNulls = false;
                            break;
                        }
                        else
                        {
                            hasOne = true;
                            onePosition = new Tuple<int, int>(i, j);
                        }
                    }
                    else if (newMatrix[i][j] != 0)
                    {
                        hasOnlyNulls = false;
                        break;
                    }


                }

                if (hasOnlyNulls && hasOne)
                {
                    hasBasicVar[onePosition.Item2] = true;
                    C[ci] = onePosition.Item1;
                    ci++;
                }

            }

            m = new bool[newMatrix.Length];

            for (int i = 0; i < newMatrix.Length; i++)
            {
                m[i] = false;
            }

            for (int i = 0; i < constraints.Length; i++)
            {

                if (!hasBasicVar[i])
                {

                    double[] basicColumn = new double[constraints.Length];

                    for (int j = 0; j < constraints.Length; j++)
                    {
                        basicColumn[j] = j == i ? 1 : 0;
                    }

                    newMatrix = appendColumn(newMatrix, basicColumn);
                    m = append(m, true);
                    C[ci] = newMatrix.Length - 1;
                    ci++;
                }

            }

            this.b = Bs;
            this.matrix = newMatrix;
        }
    }
}
