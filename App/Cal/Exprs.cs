﻿using System;

namespace Mmdo.src.Cal
{
    public class Contr
    {
        public double[] vars;
        public double b;
        public string sign;


        public Contr(double[] variables, double b, string sign = ">=")
        {
            this.vars = variables;
            this.b = b;
            this.sign = sign;
        }
    }


}
