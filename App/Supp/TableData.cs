﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mmdo.src.Cal
{
    public enum MethodType { Simplex, Gomory };
    public class TableData
    {
        public SmplxTable table;
        public MethodType MethodType;
        public TableAnswerType Result;

        public TableData(SmplxTable tableScreenshot, MethodType MethodType, TableAnswerType result)
        {
            table = tableScreenshot;
            this.MethodType = MethodType;
            Result = result;
        }

    }
}
