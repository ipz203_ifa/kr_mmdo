﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mmdo.src.Cal
{
    public class Pivot
    {
        public Tuple<int, int> index;
        public TableAnswerType result;

        public Pivot(Tuple<int, int> index, TableAnswerType result)
        {
            this.index = index;
            this.result = result;
        }
    }
}
