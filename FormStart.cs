﻿using Mmdo.src.Cal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Mmdo
{
    public partial class FormStart : Form
    {
        public FormStart()
        {
            InitializeComponent();
        }

        private void FormStart_Load(object sender, EventArgs e)
        {
            labelResult.Text = string.Empty;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            BuildExpression();
        }



        void BuildExpression()
        {

            //constraint
            List<Contr> constraints = new List<Contr    >
            {
                new Contr   ( new double[] { ToNum(numBread1.Value), ToNum(numSalo1.Value) }, ToNum(numNorm1.Value) ),
                new Contr   ( new double[] { ToNum(numBread2.Value), ToNum(numSalo2.Value) }, ToNum(numNorm2.Value) ),
                new Contr   ( new double[] { ToNum(numBread3.Value), ToNum(numSalo3.Value) }, ToNum(numNorm3.Value) ),
            };

            //func
            double[] functionVariables = new double[] { ToNum(numBreadPrice.Value), ToNum(numSaloPrice.Value) };


            SmplxMethod simplex = new SmplxMethod(new Fun(functionVariables, false), constraints.ToArray());
            var data = simplex.GetIterations();

            DrawTable(data.Item1);
            ShowAnswer(data);
        }

        double ToNum(decimal d)
        {
            return (double)d;
        }


        void DrawTable(List<SmplxTable> snaps)
        {
            ClearTable();

            int addCols = 3;
            grid.ColumnCount = snaps.First().matrix.Length + addCols;
            grid.RowHeadersVisible = false;
            grid.ColumnHeadersVisible = false;

            for (int i = 0; i < snaps.First().matrix.Length + addCols; i++)
            {
                grid.Columns[i].Width = 65;
                grid.Columns[i].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            }

            foreach (SmplxTable snap in snaps)
            {
                string[] firstRow = new string[snaps.First().matrix.Length + addCols];

                firstRow[0] = "CBi";
                firstRow[1] = "Bi";
                firstRow[2] = "Xi";

                for (int i = addCols; i < snaps.First().matrix.Length + addCols; i++)
                {
                    firstRow[i] = $"A{i - 3}";
                }

                grid.Rows.Add(firstRow);

                for (int i = 0; i < snaps.First().C.Length; i++)
                {
                    string[] row = new string[snaps.First().matrix.Length + addCols];
                    for (int j = 0; j < snaps.First().matrix.Length + addCols; j++)
                    {
                        if (j == 1)
                        {
                            row[j] = $"X{snap.C[i] + 1}";
                        }
                        else if (j == 0)
                        {
                            row[j] = snap.m[snap.C[i]] ? "-M" : $"{snap.fVars[snap.C[i]]}";
                        }
                        else if (j == 2)
                        {
                            row[j] = Round(snap.b[i]).ToString();
                        }
                        else
                        {
                            row[j] = Round(snap.matrix[j - addCols][i]).ToString();
                        }
                    }
                    grid.Rows.Add(row);
                }
                string[] fRow = new string[snaps.First().matrix.Length + addCols];

                fRow[1] = "F";
                fRow[2] = Round(snap.fValue).ToString();
                for (int i = addCols; i < snaps.First().matrix.Length + addCols; i++)
                {
                    fRow[i] = Round(snap.F[i - addCols]).ToString();
                }
                grid.Rows.Add(fRow);


                string[] emptyRow = new string[snaps.First().matrix.Length + addCols];
                grid.Rows.Add(emptyRow);
                grid.Rows.Add(emptyRow);
            }
        }


        void ShowAnswer(Tuple<List<SmplxTable>, TableAnswerType> result)
        {
            switch (result.Item2)
            {
                case TableAnswerType.Found:
                    {
                        var x1 = Round(GetValueOfX(result.Item1.Last(), 0));
                        var x2 = Round(GetValueOfX(result.Item1.Last(), 1));
                        var f = Math.Abs(Round(result.Item1.Last().fValue));

                        labelResult.Text = "Відповідь:              Отже, потрібно:\n" +
                            $"F(min) = {f}          " +
                            $"{x1} хліба та {x2} сала на суму {f} у.о.\n" +
                            $"X1 = {x1} \n" +
                            $"X2 = {x2} \n\n";
                        break;
                    }
                case TableAnswerType.Unbounded:
                    labelResult.Text = "Область допустимих \nзначень необмежена";
                    break;
                case TableAnswerType.NotYetFound:
                    labelResult.Text = "Нема оптимального \nрозв'язку";
                    break;
            }
        }

        double GetValueOfX(SmplxTable result, int id)
        {
            for (int i = 0; i < result.C.Length; i++)
            {
                if (result.C[i] == id)
                {
                    return result.b[i];
                }
            }
            return 0;
        }


        double Round(double a)
        {
            return Math.Round(a, 2);
        }

        void ClearTable()
        {
            grid.Rows.Clear();
        }

        private void grid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void labelResult_Click(object sender, EventArgs e)
        {

        }
    }
}
