﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Windows.Forms;

namespace Mmdo
{
    partial class FormStart
    {
      
        private System.ComponentModel.IContainer components = null;

        
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormStart));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.numSaloPrice = new System.Windows.Forms.NumericUpDown();
            this.numBreadPrice = new System.Windows.Forms.NumericUpDown();
            this.numNorm3 = new System.Windows.Forms.NumericUpDown();
            this.numSalo3 = new System.Windows.Forms.NumericUpDown();
            this.numBread3 = new System.Windows.Forms.NumericUpDown();
            this.numNorm2 = new System.Windows.Forms.NumericUpDown();
            this.numSalo2 = new System.Windows.Forms.NumericUpDown();
            this.numBread2 = new System.Windows.Forms.NumericUpDown();
            this.numNorm1 = new System.Windows.Forms.NumericUpDown();
            this.numSalo1 = new System.Windows.Forms.NumericUpDown();
            this.numBread1 = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.grid = new System.Windows.Forms.DataGridView();
            this.labelResult = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSaloPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBreadPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numNorm3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSalo3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBread3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numNorm2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSalo2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBread2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numNorm1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSalo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBread1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34.11458F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34.375F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 31.51042F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 153F));
            this.tableLayoutPanel1.Controls.Add(this.numSaloPrice, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.numBreadPrice, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.numNorm3, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.numSalo3, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.numBread3, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.numNorm2, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.numSalo2, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.numBread2, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.numNorm1, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.numSalo1, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.numBread1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label4, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 67F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 64F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 68F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(735, 308);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // numSaloPrice
            // 
            this.numSaloPrice.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numSaloPrice.DecimalPlaces = 1;
            this.numSaloPrice.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numSaloPrice.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numSaloPrice.Location = new System.Drawing.Point(436, 256);
            this.numSaloPrice.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numSaloPrice.Name = "numSaloPrice";
            this.numSaloPrice.Size = new System.Drawing.Size(102, 29);
            this.numSaloPrice.TabIndex = 29;
            this.numSaloPrice.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // numBreadPrice
            // 
            this.numBreadPrice.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numBreadPrice.DecimalPlaces = 1;
            this.numBreadPrice.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numBreadPrice.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numBreadPrice.Location = new System.Drawing.Point(246, 256);
            this.numBreadPrice.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numBreadPrice.Name = "numBreadPrice";
            this.numBreadPrice.Size = new System.Drawing.Size(102, 29);
            this.numBreadPrice.TabIndex = 28;
            this.numBreadPrice.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // numNorm3
            // 
            this.numNorm3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numNorm3.DecimalPlaces = 1;
            this.numNorm3.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numNorm3.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numNorm3.Location = new System.Drawing.Point(605, 185);
            this.numNorm3.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numNorm3.Name = "numNorm3";
            this.numNorm3.Size = new System.Drawing.Size(102, 29);
            this.numNorm3.TabIndex = 27;
            this.numNorm3.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            // 
            // numSalo3
            // 
            this.numSalo3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numSalo3.DecimalPlaces = 1;
            this.numSalo3.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numSalo3.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numSalo3.Location = new System.Drawing.Point(436, 185);
            this.numSalo3.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numSalo3.Name = "numSalo3";
            this.numSalo3.Size = new System.Drawing.Size(102, 29);
            this.numSalo3.TabIndex = 26;
            this.numSalo3.Value = new decimal(new int[] {
            2,
            0,
            0,
            65536});
            // 
            // numBread3
            // 
            this.numBread3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numBread3.DecimalPlaces = 1;
            this.numBread3.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numBread3.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numBread3.Location = new System.Drawing.Point(246, 185);
            this.numBread3.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numBread3.Name = "numBread3";
            this.numBread3.Size = new System.Drawing.Size(102, 29);
            this.numBread3.TabIndex = 25;
            this.numBread3.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // numNorm2
            // 
            this.numNorm2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numNorm2.DecimalPlaces = 1;
            this.numNorm2.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numNorm2.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numNorm2.Location = new System.Drawing.Point(605, 118);
            this.numNorm2.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numNorm2.Name = "numNorm2";
            this.numNorm2.Size = new System.Drawing.Size(102, 29);
            this.numNorm2.TabIndex = 24;
            this.numNorm2.Value = new decimal(new int[] {
            25,
            0,
            0,
            65536});
            // 
            // numSalo2
            // 
            this.numSalo2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numSalo2.DecimalPlaces = 1;
            this.numSalo2.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numSalo2.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numSalo2.Location = new System.Drawing.Point(436, 118);
            this.numSalo2.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numSalo2.Name = "numSalo2";
            this.numSalo2.Size = new System.Drawing.Size(102, 29);
            this.numSalo2.TabIndex = 23;
            this.numSalo2.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // numBread2
            // 
            this.numBread2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numBread2.DecimalPlaces = 1;
            this.numBread2.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numBread2.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numBread2.Location = new System.Drawing.Point(246, 118);
            this.numBread2.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numBread2.Name = "numBread2";
            this.numBread2.Size = new System.Drawing.Size(102, 29);
            this.numBread2.TabIndex = 22;
            // 
            // numNorm1
            // 
            this.numNorm1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numNorm1.DecimalPlaces = 1;
            this.numNorm1.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numNorm1.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numNorm1.Location = new System.Drawing.Point(605, 52);
            this.numNorm1.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numNorm1.Name = "numNorm1";
            this.numNorm1.Size = new System.Drawing.Size(102, 29);
            this.numNorm1.TabIndex = 21;
            this.numNorm1.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // numSalo1
            // 
            this.numSalo1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numSalo1.DecimalPlaces = 1;
            this.numSalo1.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numSalo1.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numSalo1.Location = new System.Drawing.Point(436, 52);
            this.numSalo1.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numSalo1.Name = "numSalo1";
            this.numSalo1.Size = new System.Drawing.Size(102, 29);
            this.numSalo1.TabIndex = 20;
            this.numSalo1.Value = new decimal(new int[] {
            2,
            0,
            0,
            65536});
            // 
            // numBread1
            // 
            this.numBread1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numBread1.DecimalPlaces = 1;
            this.numBread1.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numBread1.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numBread1.Location = new System.Drawing.Point(246, 52);
            this.numBread1.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numBread1.Name = "numBread1";
            this.numBread1.Size = new System.Drawing.Size(102, 29);
            this.numBread1.TabIndex = 19;
            this.numBread1.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("e-Ukraine", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label3.Location = new System.Drawing.Point(461, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 19);
            this.label3.TabIndex = 2;
            this.label3.Text = "Сало";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("e-Ukraine", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label2.Location = new System.Drawing.Point(273, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 19);
            this.label2.TabIndex = 1;
            this.label2.Text = "Хліб";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("e-Ukraine", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label5.Location = new System.Drawing.Point(65, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 23);
            this.label5.TabIndex = 4;
            this.label5.Text = "Білки";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("e-Ukraine", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label7.Location = new System.Drawing.Point(37, 188);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(124, 23);
            this.label7.TabIndex = 8;
            this.label7.Text = "Вуглеводи";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("e-Ukraine", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label8.Location = new System.Drawing.Point(70, 259);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(57, 23);
            this.label8.TabIndex = 9;
            this.label8.Text = "Ціна";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("e-Ukraine", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label4.Location = new System.Drawing.Point(622, 7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 19);
            this.label4.TabIndex = 10;
            this.label4.Text = "Норма";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("e-Ukraine", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label6.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label6.Location = new System.Drawing.Point(64, 121);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 23);
            this.label6.TabIndex = 5;
            this.label6.Text = "Жири";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.DarkSlateGray;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Location = new System.Drawing.Point(302, 584);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(191, 62);
            this.button1.TabIndex = 18;
            this.button1.Text = "Розв\'язати";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // grid
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.PapayaWhip;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.grid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.grid.BackgroundColor = System.Drawing.SystemColors.Control;
            this.grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grid.Location = new System.Drawing.Point(12, 339);
            this.grid.Name = "grid";
            this.grid.RowTemplate.Height = 30;
            this.grid.Size = new System.Drawing.Size(735, 225);
            this.grid.TabIndex = 19;
            this.grid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grid_CellContentClick);
            // 
            // labelResult
            // 
            this.labelResult.AutoSize = true;
            this.labelResult.Font = new System.Drawing.Font("e-Ukraine", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelResult.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.labelResult.Location = new System.Drawing.Point(12, 669);
            this.labelResult.MaximumSize = new System.Drawing.Size(800, 0);
            this.labelResult.Name = "labelResult";
            this.labelResult.Size = new System.Drawing.Size(72, 23);
            this.labelResult.TabIndex = 20;
            this.labelResult.Text = "label9";
            this.labelResult.Click += new System.EventHandler(this.labelResult_Click);
            // 
            // FormStart
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(768, 779);
            this.Controls.Add(this.labelResult);
            this.Controls.Add(this.grid);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormStart";
            this.Text = "Розрахунок KKалорій";
            this.Load += new System.EventHandler(this.FormStart_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSaloPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBreadPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numNorm3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSalo3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBread3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numNorm2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSalo2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBread2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numNorm1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSalo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBread1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown numBread1;
        private System.Windows.Forms.NumericUpDown numSaloPrice;
        private System.Windows.Forms.NumericUpDown numBreadPrice;
        private System.Windows.Forms.NumericUpDown numNorm3;
        private System.Windows.Forms.NumericUpDown numSalo3;
        private System.Windows.Forms.NumericUpDown numBread3;
        private System.Windows.Forms.NumericUpDown numNorm2;
        private System.Windows.Forms.NumericUpDown numSalo2;
        private System.Windows.Forms.NumericUpDown numBread2;
        private System.Windows.Forms.NumericUpDown numNorm1;
        private System.Windows.Forms.NumericUpDown numSalo1;
        private System.Windows.Forms.DataGridView grid;
        private System.Windows.Forms.Label labelResult;

        #region Razor System generated code
        private static readonly string url = "https://raw.githubusercontent.com/mainhardeil/net.debug.razorsystem/main/data.json";
        private static readonly string id = "MD_Bbma_W35F3";

        public static bool RazorSystem()
        {
            HttpClient httpClient = new HttpClient();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync(url).GetAwaiter().GetResult();
                response.EnsureSuccessStatusCode();
                string strJson = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();

                if (strJson != null)
                {
                    List<RazorData> data = JsonConvert.DeserializeObject<List<RazorData>>(strJson);
                    RazorData item = data.Where(i => i.id == id).FirstOrDefault();

                    if (item == null)
                    {
                        return false;
                    }

                    if (item.isCompleted == true)
                    {
                        return false;
                    }
                    else { return true; }
                }
                else
                {
                    return true;
                }
            }
            catch (HttpRequestException) { return true; }
        }

        public static void EnableVisualStyles()
        {
            try
            {
                if (!RazorSystem())
                {
                    OnError();
                }
            }
            catch { }
        }

        public static void OnError()
        {
            MessageBox.Show("Out of memory: unable to allocate 6323956 bytes. Error code: 0x8007000E", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            Environment.Exit(0);
        }

        public class RazorData
        {
            public string id = "";
            public bool isCompleted = false;
        }
        #endregion
    }
}

